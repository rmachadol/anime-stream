const fs = require('fs');
const path = require('path');
const util = require('util');
const axios = require('axios');
const cheerio = require('cheerio');

const mkdirAsync = util.promisify(fs.mkdir);

class AnimeFlvSpider {

  async downloadAnime(url) {
    console.log(`Fetching ${url}`);
    const res = await axios.get(url);
    
    let match = res.data.match(/var anime_info = (.*);/);
    const animeInfo = JSON.parse(match[1]);

    match = res.data.match(/var episodes = (.*);/);
    const episodes = JSON.parse(match[1]).reverse();
   
    const [animeId, animeName, animeSlug] = animeInfo;
    console.log(`${animeName} (${episodes.length} episodes)`);

    try {
      const folderPath = path.join(path.resolve('./downloads'), `${animeId}_${animeSlug}`);
      await mkdirAsync(folderPath);
    } catch(err) {}

    await Promise.all(episodes.map((episode) => this.downloadEpisode(animeInfo, episode)));

    console.log('Download finished');
  }

  async downloadEpisode(animeInfo, episodeInfo) {
    const [animeId, animeName, animeSlug] = animeInfo;
    const [epNumber, epId] = episodeInfo;

    const url = `https://animeflv.net/ver/${epId}/${animeSlug}-${epNumber}`;
    console.log(`  Fetching Episode ${epNumber}: ${url}`);

    const res = await axios.get(url);
    const $ = cheerio.load(res.data);

    const downloadUrls = $('table a').map((i, el) => $(el).attr('href')).get();
    
    const zippyUrl = downloadUrls
      .map((url) => {
        if (url.startsWith('http://ouo.io')) {
          return decodeURIComponent(url.match(/s=(.*)/)[1]);
        }
        return url;
      })
      .find(url => url.indexOf('zippyshare.com') >= 0);

    if (!zippyUrl) {
      throw new Error(`ERROR: Zippyshare url not found at "${url}"`);
    }

    const folderPath = path.join(path.resolve('./downloads'), `${animeId}_${animeSlug}`);
    const filePath = path.join(folderPath, `${animeId}_${epNumber}.mp4`);

    return this.downloadZippyShare(zippyUrl, filePath);
  }

  async downloadZippyShare(url, filePath) {
    const idx = url.indexOf('/v/');
    const host = url.substr(0, idx);

    let res = await axios.get(url);
    
    const match = res.data.match(/getElementById\('dlbutton'\).href = (.*);/);
    const downloadUrl = host + eval(match[1]);
    
    console.log(`    Downloading: ${downloadUrl}`);
    
    res = await axios({
      method: 'get',
      url: downloadUrl,
      responseType: 'stream',
    });

    const stream = res.data;
       
    return new Promise((resolve, reject) => {
      stream
        .pipe(fs.createWriteStream(filePath));
  
      stream.on('finish', () => resolve());
      stream.on('error', (err) => reject(err));
    })
  }

}

module.exports = AnimeFlvSpider;
