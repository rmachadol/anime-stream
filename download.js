const AnimeFlvSpider = require('./spiders/animeflv');

module.exports = function(url) {
  let spider;
  
  if (url.startsWith('https://animeflv.net')) {
    spider = new AnimeFlvSpider();
  }
  
  if (!spider) {
    console.log(`ERROR: No downloader found for url: "${url}"`);
    return;
  }

  return spider.downloadAnime(url);
}