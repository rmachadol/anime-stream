# Anime Downloader

## Usage

```bash
npm start download <animeUrl>
```

E.g.: `npm start download https://animeflv.net/anime/5571/one-punch-man-2nd-season`

## Currently supported sources:

* AnimeFLV