const download = require('./download');

function main() {
  const args = process.argv.slice(2);

  if (args[0] === 'download') {
    download(args[1]);
  } else {
    console.log('USE: npm start download <url>');
  }
}

main();
